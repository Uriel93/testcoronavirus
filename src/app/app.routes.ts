import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './componentes/menu/menu.component';
import { HomeComponent } from './componentes/home/home.component';


const APP_ROUTE: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'noticias', component: MenuComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'home'}
 ];

 export const APP_ROUTING = RouterModule.forRoot(APP_ROUTE);

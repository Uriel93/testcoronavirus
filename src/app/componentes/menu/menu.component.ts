import { Component, OnInit } from '@angular/core';

// importando los modulos de inicio de sesiòn
import { AuthService } from "angularx-social-login";
import { GoogleLoginProvider, SocialUser } from "angularx-social-login";

// direccionamiento router
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  private user: SocialUser;
  private loggedIn: boolean;

  constructor(private authService: AuthService, private route: ActivatedRoute) { }

  ngOnInit(){
    // consumiendo servicio google para el header
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });

    // manipulacion de menu tipo ancla
    this.route.fragment.subscribe(f => {
      const element = document.querySelector("#" + f);
      if (element) element.scrollIntoView()
    })
  }

  // Iniciar sesion con google
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  // cerrar sesion
  signOut(): void {
    this.authService.signOut();
  }

}

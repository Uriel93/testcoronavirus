import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts';

// importando los servicios
import { serviceContagios } from '../../app.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})

export class RegistroComponent implements OnInit {

    // varibles para mostrar tabla 
    public paises: [];
    public todos: Array<any> = [];

    // variables para el grafico
    public lineChartData: Array<any> = [ { data: [56, 41, 15, 10], label: 'sector A' }];
    public lineChartLabels: Array<any> = ['Pais', 'Casos', 'Activo', 'Recuperados', 'Fallecidos'];

    public lineChartOptions:any = {
      responsive: true
    };

    // estilos del grafico
    public lineChartColors: Array<any> = [
      { // red
        backgroundColor: 'rgba(255,0,0,0.3)',
        borderColor: 'red',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      },
      { // grey
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      },
      { // dark grey
        backgroundColor: 'rgba(77,83,96,0.2)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)'
      }
    ];

    public lineChartLegend = true;
    public lineChartType = 'line';

    // eredando directivas que implementa el grafico
    @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

    constructor(private contagioServ: serviceContagios ) { }

    ngOnInit(){
      // Consumiendo el servicio de contagios top 5
      this.contagioServ.optenerContagios().subscribe(
        resp => {
          for(let i = 0; i < resp['data'].length; i++){
            if(i === 5){ break; }
              let encontrar = { data: [resp['data'][i]['country'], resp['data'][i]['cases'], resp['data'][i]['active'], resp['data'][i]['recovered'], resp['data'][i]['deaths']], label: resp['data'][i]['country'] };
              this.todos.push(encontrar);
          }
          this.lineChartData = this.todos;
        }
      );
    }

    // calculando numero de registros del servicio
    public randomize(): void {
      for (let i = 0; i < this.lineChartData.length; i++) {
        for (let j = 0; j < this.lineChartData[i].data.length; j++) {
          this.lineChartData[i].data[j] = this.generateNumber(i);
        }
      }
      this.chart.update();
    }

    private generateNumber(i: number) {
      return Math.floor((Math.random() * (i < 2 ? 100 : 500)) + 1);
    }

    // events para disparar acciones de click para el grafico
      public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
      }

      public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
        console.log(event, active);
      }

}

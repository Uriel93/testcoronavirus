import { Component, OnInit } from '@angular/core';
import { serviceContagios } from '../../app.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-localitation',
  templateUrl: './localitation.component.html',
  styleUrls: ['./localitation.component.css']
})
export class LocalitationComponent implements OnInit {

  constructor(private contagioServ: serviceContagios, private formBuilder: FormBuilder) { }

  // variables para google maps
  public lat: number;
  public lng: number;
  zoom: number = 1;
  public active = true;

  // variables para informacion por pais y envios de formulario
  formu = this.formBuilder.group({pais: ['']});
  paisInf: string;
  casesInf: string;
  activeInf: string;
  recoverInf: string;
  deathsInf: string;

  ngOnInit() {
    this.active = true;

    // validacion de formulario
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();

  }

  envioPais(){
    this.active = false;

    // consumiendo servicio de busqueda por pais
    this.contagioServ.paisCon(this.formu.value['pais']).subscribe( resp  => {
      for(let i = 0; i < resp['data'].length; i++){
        if(i === 1){ break; }
        this.paisInf = resp['data'][i]['country'];
        this.casesInf = resp['data'][i]['cases'];
        this.activeInf = resp['data'][i]['active'];
        this.recoverInf = resp['data'][i]['recovered'];
        this.deathsInf = resp['data'][i]['deaths'];

        this.lat = resp['data'][i]['countryInfo'].lat;
        this.lng = resp['data'][i]['countryInfo'].long;
      }
      this.zoom = 3;
    },
    // error de servicio
    (error) => {
      var element = document.getElementById("aler");
      element.classList.add("show");
      setTimeout(() => {
        element.classList.remove("show");
      }, 2000);

      throw error;
    });
  }
}


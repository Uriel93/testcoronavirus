import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// componentes
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule} from '@angular/material';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatSelectModule} from '@angular/material/select';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';

// componentes creados
import { AppComponent } from './app.component';
import { HomeComponent } from './componentes/home/home.component';
import { LocalitationComponent } from './componentes/localitation/localitation.component';
import { RegistroComponent } from './componentes/registro/registro.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { ChartsModule } from 'ng2-charts';

//api inicio de sesion con google 
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider} from "angularx-social-login";

//route
import { APP_ROUTING } from './app.routes';

//api maps
import { AgmCoreModule } from '@agm/core';

// servicios
import { serviceContagios } from './app.service';
import { HttpClientModule } from '@angular/common/http';

// id de google maps
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("21787014572-qvbjnug3p69ojk30t7tg7hv8u0pmnh89.apps.googleusercontent.com")
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LocalitationComponent,
    RegistroComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    BrowserAnimationsModule,
    APP_ROUTING,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDOg6yqbZJRM7MbJpBAA9mFVNlOD2z6PDk'
    }),
    SocialLoginModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    serviceContagios
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

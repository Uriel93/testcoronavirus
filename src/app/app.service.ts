import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })

export class serviceContagios {

    // Top5 paises con coronavirus url
     top5 = 'https://corona-stats.online/?format=json';
    // Por pais
     pais_inf = 'https://corona-stats.online/';

    constructor(private http: HttpClient){}

    // Http Headers
      httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }

       // GET de top 5
      optenerContagios(): Observable<any> {
        return this.http.get(this.top5);
      }

      // GET por pais
      paisCon(country: any) {
        return this.http.get(this.pais_inf + country + '?format=json');
      }
}
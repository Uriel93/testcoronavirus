/*******************************/
/*******************************/
- APLICACION: "Noticias HOY"
- DESARROLLO: Angular 8
- EXTENCIONES ANGULAR: 
     a) angularx-social-login
     b) ng2-charts
     c) chart.js
     d) angular2-google-maps
     e) Boostrap
- CREADO: Ing. Abraham Uriel Zavala Garcia
- PARA: Podemos Progresar
- TIEMPO DESARROLLO: 4 dias
/*******************************/
/*******************************/

Introducciòn;

La aplicaciòn "Noticias HOY" tiene como objetivo mostrar las ultimas noticias sobre el coronavirus, mostrando como base el top 5 mundial de
contagios por pais y en su defecto una busqueda por pais, esto con el fin para que las personas sepan como se esta propagando dicha enfermedad.
Esta aplicacion se costituye por un login y un cierre de sesiòn que esta creado con el API (google) y posterior con la 
API (coronavirus/help) para extraccion de dicha informaciòn como tambien el consumo de API (google maps) para vizualisar cada pais; 
    - muestra por medio de una grafica el top 5 mundial de paises con mas contagios junto con Casos, Activos, Recuperados y Fallecidos.
    - muestra por medio de un servicio get (con parametros) una busqueda por pais para vizualisar los contagios.

Intalaciòn;

A continuaciòn te mencionare el proceso de instalaciòn;

    Se tiene como instalaciones las siguientes acciones
	a) angularx-social-login (npm install --save angularx-social-login) -- login por google
     	b) ng2-charts (npm install --save ng2-charts )  -- Implementacion de graficos
     	c) chart.js (npm install --save chart.js)  --Implementacion de graficos
     	d) angular2-google-maps (npm install angular2-google-maps --save) -- Implementacion de api de google maps
     	e) Boostrap (npm install bootstrap --save) -- Maquetacion de la aplicacion

Funcionalidad de la aplicacion (Por paginas);

Page one; 
     Aqui solo te muestra un logo y una accion para login por google esto se debe tener una cuenta de gmail.com, tambien puedes cerrar sesion
     con tu misma cuenta
		NOTA; No se podra ingresar a a la aplicacion si no se tiene una cuenta de google

Page two;
     "menu" => Aqui solo se muestra un menu con tres campos (Noticias, Ultima hora y Busqueda de contagios) 
               estas tienen una accion de ancla por cada seccion.
     "slider" => Encontraras un slider que contiene 3 notas de noticias.
     "ultima hora" => Una breve informacion del coronavirus y una grafica de top 5 del mundo de contagios junto con una tabla
     "Busqueda de contagios" => Una breve informacion de como se contagia el coronavirus y una busqueda por pais sobre el coronavirus
				dicha busqueda es en ingles si no se hace correcto lanzara un mensaje de error mencionando que debe
				ser en ingles dicho pais.

Conclusiones; 

Esta aplicacion fue un examen tecnico por "Podemos Progresar"